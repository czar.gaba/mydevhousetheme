<!-- sample theme header -->
<!DOCTYPE html>
<html <?php language_attributes(); ?>> 
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php wp_title( '|', true, 'right' ); bloginfo('name'); ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
 
  
<!--  site icon-->
<!-- <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" /> -->
  
<!--  style.css link-->
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" />
	

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!--your custom js-->

<?php 
// Fires the 'wp_head' action and gets all the scripts included by wordpress, wordpress plugins or functions.php 
// using wp_enqueue_script if it has $in_footer set to false (which is the default)
wp_head(); 
?>
  
  



</head>
<!--body class-->
<body <?php body_class(); ?>>



